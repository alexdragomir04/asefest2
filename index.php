<!DOCTYPE html>
<html>
<head>
	<title>aseFest 2018</title>
	<meta charset="UTF-8">
	<script src="style/jquery.js"></script>
	<link rel="stylesheet" href="style/bootstrap.css">
	<link rel="stylesheet" href="style/style.css">
</head>
<body>
<div id="container">
<center>
<div class="formular">
	<form>
<br>
<h2>ÎNSCRIERE ASE FEST</h2><br>
	 <div class="inp">
		 <form action="submit.php" method="post" name="first_form" id="first_form">
		  <input type="text" class="form-control" name="firstname" placeholder="Nume">
			<input type="text" class="form-control"  name="prenume" placeholder="Prenume">
			<input class="form-control" type="text" name="phone" placeholder="Numar de telefon">
			<input class="form-control" type="text" name="email" placeholder="Email">
			<input class="form-control" type="text" name="facebook" placeholder="Facebook URL">
			<input class="form-control" type="text" name="fac" placeholder="Facultate">
			<input class="form-control" type="text" name="profil" placeholder="Profil">
			<label for="andestudiu"><small>Anul de studiu</small></label>
			<select class="form-control" name="optiune">
			      <option>1</option>
			      <option>2</option>
			      <option>3</option>
			    </select>
			<center><label for="birth">Zi de nastere</label></center>
			<input class="form-control" type="date" name="birth" placeholder="Zi de nastere">
			<label for="question">De ce vrei sa participi la ASE FEST?<br><small>(maxim 50 de cuvinte)</small></label>
			<textarea class="form-control" rows="3" name="question"></textarea>
			<label for="captcha_generated">Introduceti codul captcha</label>
			<?php
			$digits = 5;
			$captcha = rand(pow(10, $digits-1), pow(10, $digits)-1);
			echo '<input class="form-control" type="text" name="captcha_generated" readonly="readonly" style="background-color:#d3d3d3;" value="';
			echo $captcha;
			echo '">';
			?>
			<input class="form-control" type="text" name="captcha_inserted" placeholder="Insert here the number">
			<button type="button" class="btn btn-primary">Trimite</button>
	  </div>
</form>
</center>
</div>
</div>
</body>
</html>

<script type="text/javascript">
//Callback handler for form submit event
$("#first_form").submit(function(e)
{
 
    var formObj = $(this);
    var formURL = formObj.attr("action");
    var formData = new FormData(this);
    $.ajax({
        url: formURL,
    type: 'POST',
        data:  formData,
    mimeType:"multipart/form-data",
    contentType: false,
        cache: false,
        processData:false,
    success: function(data, textStatus, jqXHR)
    {
         console.log(data);
    },
     error: function(jqXHR, textStatus, errorThrown)
     {
     }         
    });
    e.preventDefault(); //Prevent Default action.
});
$("#multiform").submit(); //Submit the form
</script>
